class Galery < ApplicationRecord
  mount_uploader :file, FileUploader

  before_create :set_metadata

  private

    def set_metadata
      uploaded_file = self.file
      if uploaded_file.content_type == 'image/jpeg'
        creation_date = EXIFR::JPEG.new(uploaded_file.file.file).date_time

        unless creation_date.nil?
          self.created_at = creation_date.strftime("%Y.%m.%d %I:%M%P")
        end
      end
    end
end
