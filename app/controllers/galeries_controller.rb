class GaleriesController < ApplicationController
  def index
    @galery = Galery.new
    @galery_files = Galery.all.order(created_at: :desc)
  end

  def show

  end

  def new

  end

  def create
    @galery = Galery.create!(galery_params)
    redirect_to galeries_path
  end

  def update

  end

  def destroy

  end

  private

    def galery_params
      params.require(:galery).permit(:file)
    end
end
