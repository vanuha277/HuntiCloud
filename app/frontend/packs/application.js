require.context('../images', true)

import Rails from "@rails/ujs"
import "@hotwired/turbo-rails"
import * as ActiveStorage from "@rails/activestorage"
import '../js/bootstrap_js_files.js'
import "channels"
import '../stylesheets/application.scss'

Rails.start()
ActiveStorage.start()
