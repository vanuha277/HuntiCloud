class AddFileUploadingToGalery < ActiveRecord::Migration[6.1]
  def change
    create_table :galeries do |t|
      t.string :file, null: false
    end
  end
end
