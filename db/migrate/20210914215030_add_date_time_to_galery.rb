class AddDateTimeToGalery < ActiveRecord::Migration[6.1]
  def change
    add_column :galeries, :created_at, :datetime
  end
end
