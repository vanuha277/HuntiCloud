Rails.application.routes.draw do
  devise_for :users, skip: [:password, :registrations, :sessions]
  as :user do
    get 'login', to: 'devise/sessions#new', as: :new_user_session
    post 'login', to: 'devise/sessions#create', as: :user_session
    delete 'logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  root 'home#index'

  resources :galeries
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
